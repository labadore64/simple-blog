---
title: /tutorial
layout: page
permalink: /tutorial/themes
---
# Themes
You can customize your site with hundreds of possible themes. You can read Jekyll's documentation [here](https://jekyllrb.com/docs/themes/) for more info too.

*Note: This blog is using the [jekyll-theme-console](https://github.com/b2a3e8/jekyll-theme-console) theme.*

## Finding Themes
You can find free themes on the following sites:
* [jamstackthemes.dev](https://jamstackthemes.dev/ssg/jekyll/)
* [jekyllthemes.org](http://jekyllthemes.org/)
* [jekyll-themes.com](https://jekyll-themes.com/free/)

## Installing Themes
Most themes will have instructions on how to install them. Jekyll also has more details [here](https://jekyllrb.com/docs/themes/#understanding-gem-based-themes) on how to install themes with ``gem``.

If all else fails, you can also just copy the files from the demo. This should usually work.
