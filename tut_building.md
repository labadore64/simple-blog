---
title: /tutorial
layout: page
permalink: /tutorial/building
---
# Building your Site
*Note: this is only an introductory guide. Jekyll provides a great in depth [step-by-step guide](https://jekyllrb.com/docs/step-by-step/01-setup/) on how to build your website.*

## _config.yml
This file organizes how your your website is structured. Review the file's documentation on how to fill this out for your website.

*Note: YAML files require a specific syntax. Refer [here](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html) as a guide.

## Pages
Pages are basic static pages. Pages are markdown files and can be named anything. However, you must include a header that looks like this on your pages.

```
---
title: /project
layout: page
permalink: /project
---
```
The title is the title of the page. The permalink is the relative link to the page. The layout is which layout to use in ``_layouts``.

## Posts
Posts are specialized pages that let you organize your site like a blog instead of just a static website. You can even set up your blog so that it automatically lists all your posts with dates and a scrolling feed, depending on the theme you choose.

Posts should be stored under the directory ``_posts`` as a ``.markdown`` file. They must follow the following naming format:

```
YYYY-MM-DD-[name_without_spaces].markdown
```

In the file, you must also add a header at the top. Here's an example:

```
---
layout: post
title:  "Game Development and Accessibility"
date:   2020-02-29 01:44:05 -0500
tags: needs tech
---
```

Anything below this will be part of your post, and should be written in markdown.

*Note: layout indicates that it will use the ``post.html`` layout found in ``_layouts``.*
# Building Pages
Pages are written in markdown.

## Adding Code
You can add inline code to your site with the following syntax:

```
``printf("Inline code!");``
```

This will highlight the code syntax separately like this: ``printf("Inline code!");``

To do a block of code, do this syntax (with the asterisk removed:

```
*```
printf("Inline code!");
*```
```

The end result will look like this:

```
printf("Inline code!");
```

## Images
There are two ways to post images.
To do an inline image:

```
![alt text](/images/example_image.png "Hover text")
```

To do a reference-style image:

```
![alt text][logo]

[logo]: /images/example_image.png "Hover text"
```

The end result in either case will look similar to this:

![A cat on the left, and a chameleon on the right. Both are black and white. The cat has a white face with the rest of his head black, and a hair tuft and white tipped ears. He wears glasses. He wears a sweater and pants, with a scarf. He has white paws and a black tail with two rings on it. The chameleon has a round head with 3 horns, with a light underbelly. He has a sharp skull with several pertrusions pointing back. He also is wearing a sweater and a scarf. His tail is long and curls back behind him.][logo]

*Note: Don't forget to add alt text! Describe the image meaningfully to convey to users who can't view the image.*

## Download Links
If you want to share a download link, you can save your file to your repository, and either reference it in-line style:

```
[Essay](/downloads/picture.jpg)
```

Or reference style:

```
[Essay][download]
[download]: /downloads/picture.jpg
```

The end result will look like this:
[Essay][download]

# Testing Your Site
To test your site, you can deploy it locally by executing the following:

```
bundle exec jekyll serve
```

If all goes well, you can view your local website in the link the console provides you. If it gives you an error about needing to install the bundle packages, try running the following before trying again:

```
bundle install
```


[logo]: /images/example_image.png "Cat and Chameleon"
[download]: /downloads/picture.jpg
